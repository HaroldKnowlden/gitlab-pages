module gitlab.com/gitlab-org/gitlab-pages/tools

go 1.16

require (
	github.com/golang/mock v1.3.1
	github.com/golangci/golangci-lint v1.27.0
	gotest.tools/gotestsum v1.7.0
)
